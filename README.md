# manifests
Build Slim8 for i9100 without the patience of universe

```
1. mkdir -p slim8

2. cd slim8

3. repo init -u git://github.com/SlimRoms/platform_manifest.git -b or8.1

4. git clone https://gitlab.com/getawaycar/isolatedworld/nopatience/manifests.git -b master .repo/local_manifests

5. repo sync --no-tags --no-clone-bundle --force-sync -c

6. To build:
  . build/envsetup.sh
  lunch (choose i9100)
  brunch i9100
```

Link of project: https://forum.xda-developers.com/galaxy-s2/development-derivatives/rom-slim7-t3739501
